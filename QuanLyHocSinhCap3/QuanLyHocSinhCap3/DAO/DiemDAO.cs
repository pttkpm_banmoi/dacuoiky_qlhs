﻿using QuanLyHocSinhCap3.Database;
using QuanLyHocSinhCap3.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace QuanLyHocSinhCap3.DAO
{
    public class DiemDAO
    {
        public List<DiemDTO> ReadAll()
        {
            var listDB_Diem = new List<DiemDTO>();
            using (var db = new ProcessData().ConnectionDB())
            {
                var table = from diem in db.DIEMs select diem;
                foreach (var item in table)
                {
                    var tmp = new DiemDTO();
                    tmp.HocSinh = new HocSinhDTO();
                    tmp.HocKy = new HocKyDTO();
                    tmp.MonHoc = new MonHocDTO();
                    tmp.HocSinh.Lop = new LopDTO();
                    tmp.HocSinh.ID = item.IDHS;
                    tmp.HocSinh.Lop.ID = item.HOCSINH.LOP.IDLOP;
                    tmp.HocSinh.Ten = item.HOCSINH.TENHS;
                    tmp.HocKy.ID = item.IDHK;
                    tmp.MonHoc.ID = item.IDMH;
                    tmp.MonHoc.Ten = item.MONHOC.TENMH;
                    tmp.Diem15P = item.DIEM15P;
                    tmp.Diem1Tiet = item.DIEM1TIET;
                    tmp.DiemHocKy = item.DIEMHOCKY;
                    tmp.DiemTongKet = new DiemDAO().TongPhay(item);
                    listDB_Diem.Add(tmp);
                }
            }
            return listDB_Diem;
        }
        public List<DiemDTO> ReadByIDHocSinh(String IDHOCSINH)
        {
            var ListDiem = new List<DiemDTO>();
            using (var db = new ProcessData().ConnectionDB())
            {            
                var tmp = (from d in db.DIEMs where d.HOCSINH.IDHS == IDHOCSINH select d).ToArray();
                foreach(var item in tmp)
                {
                    var diem = new DiemDTO();
                diem.HocKy = new HocKyDTO();
                diem.HocSinh = new HocSinhDTO();
                diem.HocSinh.Lop = new LopDTO();
                diem.MonHoc = new MonHocDTO();
                    diem.HocSinh.Lop.ID = item.HOCSINH.LOP.IDLOP;
                    diem.HocKy.ID = item.IDHK;
                    diem.HocSinh.ID = item.IDHS;
                    diem.MonHoc.ID = item.IDMH;
                    diem.Diem15P = item.DIEM15P;
                    diem.Diem1Tiet = item.DIEM1TIET;
                    diem.DiemHocKy = item.DIEMHOCKY;
                    ListDiem.Add(diem); 
                }    
            }
            return ListDiem;
        }
        public List<DiemDTO> ReadByIDMonHoc(String IDMonHoc)
        {
            var listDB_Diem = new List<DiemDTO>();
            using (var db = new ProcessData().ConnectionDB())
            {
                var table = from diem in db.DIEMs select diem;
                foreach (var item in table)
                {
                    if (item.IDMH == IDMonHoc)
                    {
                        var tmp = new DiemDTO();
                        tmp.HocSinh = new HocSinhDTO();
                        tmp.HocKy = new HocKyDTO();
                        tmp.MonHoc = new MonHocDTO();
                        tmp.HocSinh.ID = item.IDHS;
                        tmp.HocKy.ID = item.IDHK;
                        tmp.MonHoc.ID = item.IDMH;
                        tmp.Diem15P = item.DIEM15P;
                        tmp.Diem1Tiet = item.DIEM1TIET;
                        tmp.DiemHocKy = item.DIEMHOCKY;
                        listDB_Diem.Add(tmp);
                    }
                }
            }
            return listDB_Diem;
        }
        public List<DiemDTO> ReadByIDHocKy(String IDHocKy)
        {
            var listDB_Diem = new List<DiemDTO>();
            using (var db = new ProcessData().ConnectionDB())
            {
                var table = from diem in db.DIEMs select diem;
                foreach (var item in table)
                {
                    if (item.IDHK == IDHocKy)
                    {
                        var tmp = new DiemDTO();
                        tmp.HocSinh = new HocSinhDTO();
                        tmp.HocKy = new HocKyDTO();
                        tmp.MonHoc = new MonHocDTO();
                        tmp.HocSinh.ID = item.IDHS;
                        tmp.HocKy.ID = item.IDHK;
                        tmp.MonHoc.ID = item.IDMH;
                        tmp.Diem15P = item.DIEM15P;
                        tmp.Diem1Tiet = item.DIEM1TIET;
                        tmp.DiemHocKy = item.DIEMHOCKY;
                        listDB_Diem.Add(tmp);
                    }
                }
            }
            return listDB_Diem;
        }
        public List<DiemDTO> ReadDiemByIDHS_IDMH_IDHK_IDLOP(String IDHS, String IDMH, String IDHK,String IDLOP)
        {
            var ListDiem = new List<DiemDTO>();
            var tmp = new List<DIEM>();
            using (var db = new ProcessData().ConnectionDB())
            {       
                if (IDHS != "" && IDHK !="" && IDLOP !="" && IDMH != "")
                {
                     tmp = (from d in db.DIEMs where (d.HOCSINH.IDHS == IDHS && d.HOCKY.IDHK == IDHK && d.HOCSINH.LOP.IDLOP == IDLOP
                                                  &&  d.MONHOC.IDMH == IDMH)
                                               select d).ToList();
                }

                if (IDHS == "" && IDHK != "" && IDLOP != "" && IDMH != "")
                {
                    tmp = (from d in db.DIEMs
                           where (d.HOCKY.IDHK == IDHK && d.HOCSINH.LOP.IDLOP == IDLOP
                              && d.MONHOC.IDMH == IDMH)
                           select d).ToList();
                }
                if (IDHS == "" && IDHK == "" && IDLOP != "" && IDMH != "")
                {
                    tmp = (from d in db.DIEMs
                           where (d.HOCSINH.LOP.IDLOP == IDLOP
                              && d.MONHOC.IDMH == IDMH)
                           select d).ToList();
                }
                if (IDHS == "" && IDHK == "" && IDLOP == "" && IDMH != "")
                {
                    tmp = (from d in db.DIEMs
                           where (
d.MONHOC.IDMH == IDMH)
                           select d).ToList();
                }

                if (IDHS != "" && IDHK == "" && IDLOP != "" && IDMH != "")
                {
                    tmp = (from d in db.DIEMs
                           where (d.HOCSINH.IDHS == IDHS && d.HOCSINH.LOP.IDLOP == IDLOP
                              && d.MONHOC.IDMH == IDMH)
                           select d).ToList();
                }
                if (IDHS != "" && IDHK == "" && IDLOP == "" && IDMH != "")
                {
                    tmp = (from d in db.DIEMs
                           where (d.HOCSINH.IDHS == IDHS
                              && d.MONHOC.IDMH == IDMH)
                           select d).ToList();
                }
                if (IDHS != "" && IDHK == "" && IDLOP == "" && IDMH == "")
                {
                    tmp = (from d in db.DIEMs
                           where (d.HOCSINH.IDHS == IDHS)
                           select d).ToList();
                }

                if (IDHS != "" && IDHK != "" && IDLOP == "" && IDMH != "")
                {
                    tmp = (from d in db.DIEMs
                           where (d.HOCSINH.IDHS == IDHS && d.HOCKY.IDHK == IDHK
                              && d.MONHOC.IDMH == IDMH)
                           select d).ToList();
                }
                if (IDHS != "" && IDHK != "" && IDLOP == "" && IDMH == "")
                {
                    tmp = (from d in db.DIEMs
                           where (d.HOCSINH.IDHS == IDHS && d.HOCKY.IDHK == IDHK)
                           select d).ToList();
                }

                if (IDHS != "" && IDHK != "" && IDLOP != "" && IDMH == "")
                {
                    tmp = (from d in db.DIEMs
                           where (d.HOCSINH.IDHS == IDHS && d.HOCKY.IDHK == IDHK && d.HOCSINH.LOP.IDLOP == IDLOP)
                           select d).ToList();
                }

                if (IDHS == "" && IDHK == "" && IDLOP == "" && IDMH == "")
                {
                    //tmp = ReadAll();
                }

                foreach (var item in tmp)
                {            
                    var diem = new DiemDTO();
                    diem.HocKy = new HocKyDTO();
                    diem.HocSinh = new HocSinhDTO();
                    diem.HocSinh.Lop = new LopDTO();
                    diem.MonHoc = new MonHocDTO();
                    diem.HocSinh.Lop.ID = item.HOCSINH.LOP.IDLOP;
                    diem.HocKy.ID = item.IDHK;
                    diem.HocKy.Ten = item.HOCKY.TENHK;
                    diem.HocSinh.ID = item.IDHS;
                    diem.HocSinh.Ten = item.HOCSINH.TENHS;
                    diem.MonHoc.ID = item.IDMH;
                    diem.MonHoc.Ten = item.MONHOC.TENMH;
                    diem.Diem15P = item.DIEM15P;
                    diem.Diem1Tiet = item.DIEM1TIET;
                    diem.DiemHocKy = item.DIEMHOCKY;
                    ListDiem.Add(diem);
                }
            }
            return ListDiem;
        }
        public void Create()
        {
            using (var db = new ProcessData().ConnectionDB())
            {
                var tableHocSinh = from hocsinh in db.HOCSINHs select hocsinh;
                var tableMonHoc = from monhoc in db.MONHOCs select monhoc;
                var tableHocKy = from hocky in db.HOCKies select hocky;
                //var tableDiem = from hocsinh in db.HOCSINHs select hocsinh;
                foreach (var itemHocSinh in tableHocSinh)
                {
                    foreach(var itemMonHoc in tableMonHoc)
                    {
                        foreach(var itemHocKy in tableHocKy)
                        {
                            
                            if (db.DIEMs.Find(itemHocSinh.IDHS, itemHocKy.IDHK, itemMonHoc.IDMH) == null)
                            {
                                DIEM diem = new DIEM();
                                /*diem.HOCSINH.IDHS = itemHocSinh.IDHS;
                                diem.HOCSINH.LOP.IDLOP = ite*/
                                //diem.HOCSINH = itemHocSinh;
                                diem.IDHS = itemHocSinh.IDHS;
                                diem.IDHK = itemHocKy.IDHK;
                                diem.IDMH = itemMonHoc.IDMH;
                                diem.DIEM15P = "0";
                                diem.DIEM1TIET = "0";
                                diem.DIEMHOCKY = "0";
                                db.DIEMs.Add(diem);
                            }
                        }
                    }  
                }
                db.SaveChanges();
            }
        }
        public void Insert(DIEM diem)
        {
            using (var db = new ProcessData().ConnectionDB())
            {
                db.DIEMs.Add(diem);
                db.SaveChanges();
            }
        }
        public void Update(string ID, string IDHK, string IDMH, DIEM diem)
        {
            using (var db = new ProcessData().ConnectionDB())
            {
                var diemupdate = db.DIEMs.Find(ID, IDHK, IDMH);
                // hocsinhupdate.IDHS = hocsinh.IDHS;
                diemupdate.DIEM15P = diem.DIEM15P;
                diemupdate.DIEM1TIET = diem.DIEM1TIET;
                diemupdate.DIEMHOCKY = diem.DIEMHOCKY;
                diemupdate.IDHK = diem.IDHK;
                diemupdate.IDHS = diem.IDHS;
                diemupdate.IDMH = diem.IDMH;
                db.SaveChanges();
            }
        }
        public void Delete(DIEM diem)
        {
            using (var db = new ProcessData().ConnectionDB())
            {
                var delete = db.DIEMs.Find(diem.IDHS, diem.IDHK, diem.IDMH);
                db.DIEMs.Remove(delete);
                db.SaveChanges();
            }
        }
        public string TongPhay(DIEM diem)
        {     
            var result = 0.0;
            var heso = 0;
            try { var diem15p = diem.DIEM15P.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var diems in diem15p)
                {
                    float rs;
                    bool res;
                    res = float.TryParse(diems, out rs);
                    if (res == false)
                    {
                        MessageBox.Show(" value is not false");
                    }
                    else
                    {
                        result = result + float.Parse(diems);
                        heso++;
                    }
                }
            }
            catch { result = result; }
            try
            {
                var diem1tiet = diem.DIEM1TIET.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var diems in diem1tiet)
                {
                    float rs;
                    bool res;
                    res = float.TryParse(diems, out rs);
                    if (res == false)
                    {
                        MessageBox.Show(" value is not false");
                    }
                    else
                    {
                        result = result + float.Parse(diems)*2;
                        heso = heso + 2;
                    }
                }
            }
            catch { result = result; }
            try
            {
                var diemhocky = diem.DIEMHOCKY.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var diems in diemhocky)
                {
                    float rs;
                    bool res;
                    res = float.TryParse(diems, out rs);
                    if (res == false)
                    {
                        MessageBox.Show(" value is not false");
                    }
                    else
                    {
                        result = result + float.Parse(diems)*3;
                        heso = heso + 3;
                    }
                }
            }
            catch { result = result; }
            if (heso == 0)
            {
                return 0.ToString();
            }
            return (result/heso).ToString();
        }
    }
}
