﻿using QuanLyHocSinhCap3.Database;
using QuanLyHocSinhCap3.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyHocSinhCap3.DAO
{
    public class HocSinhDAO
    {
        public List<HocSinhDTO> ReadAll()
        {
            var listDB_HocSinh = new List<HocSinhDTO>();
            using (var db = new ProcessData().ConnectionDB())
            {
                var table = from hocsinh in db.HOCSINHs select hocsinh;
                foreach (var item in table)
                {
                    try
                    {
                        var tmp = new HocSinhDTO();
                        tmp.Lop = new LopDTO();
                        tmp.ID = item.IDHS;
                        tmp.Ten = item.TENHS;
                        tmp.GioiTinh = item.GIOITINH;
                        tmp.NgaySinh = item.NGAYSINH.Value;
                        tmp.Lop.ID = item.IDLOP;
                        tmp.Lop.Ten = item.LOP.TENLOP;
                        tmp.DiaChi = item.DIACHI;
                        tmp.Email = item.EMAIL;
                        listDB_HocSinh.Add(tmp);
                    }
                    catch
                    {

                    }
                }
            }
            return listDB_HocSinh;
        }
        public List<HocSinhDTO> ReadByIDHocSinh(string ID)
        {
            HocSinhDTO hs = new HocSinhDTO();
            List<HocSinhDTO> lisths = new List<HocSinhDTO>();
            using (var db = new ProcessData().ConnectionDB())
            {
                var item = (from h in db.HOCSINHs where h.IDHS == ID select h).Single();
                try
                {
                    var tmp = new HocSinhDTO();
                    tmp.Lop = new LopDTO();
                    tmp.ID = item.IDHS;
                    tmp.Ten = item.TENHS;
                    tmp.GioiTinh = item.GIOITINH;
                    tmp.NgaySinh = item.NGAYSINH.Value;
                    tmp.Lop.ID = item.IDLOP;
                    tmp.Lop.Ten = item.LOP.TENLOP;
                    tmp.DiaChi = item.DIACHI;
                    tmp.Email = item.EMAIL;
                    lisths.Add(tmp);
                }
                catch
                {

                }
            }
            return lisths;
        }
        public void Insert(HOCSINH hocsinh)
        {
            using (var db = new ProcessData().ConnectionDB())
            {
                db.HOCSINHs.Add(hocsinh);
                var lop = (from item in db.LOPs where item.IDLOP == hocsinh.IDLOP select item).Single();
                lop.SISO++;
                db.SaveChanges();
            }
        }
        public void Delete(HocSinhDTO hocsinh)
        {
            using (var db = new ProcessData().ConnectionDB())
            {
                var delete = db.HOCSINHs.Find(hocsinh.ID);
                db.HOCSINHs.Remove(delete);
                var lop = (from item in db.LOPs where item.IDLOP == hocsinh.Lop.ID select item).Single();
                lop.SISO--;
                var diem = (from item in db.DIEMs where item.IDHS == hocsinh.ID select item).ToList();
                foreach( var itm in diem)
                {
                    db.DIEMs.Remove(itm);
                }

                db.SaveChanges();
            }
        }
        public void Update(string ID, HOCSINH hocsinh)
        {
            using (var db = new ProcessData().ConnectionDB())
            {
                var hocsinhupdate = db.HOCSINHs.Find(ID);
                // hocsinhupdate.IDHS = hocsinh.IDHS;
                hocsinhupdate.TENHS = hocsinh.TENHS;
                hocsinhupdate.GIOITINH = hocsinh.GIOITINH;
                hocsinhupdate.NGAYSINH = hocsinh.NGAYSINH;
                hocsinhupdate.EMAIL = hocsinh.EMAIL;
                hocsinhupdate.DIACHI = hocsinh.DIACHI;
                hocsinhupdate.IDLOP = hocsinh.IDLOP;
                db.SaveChanges();
            }
        }
        public List<HocSinhDTO> ReadByIDLop(string IDLop)
        {
            var listDB_HocSinh = new List<HocSinhDTO>();
            using (var db = new ProcessData().ConnectionDB())
            {
                var table = from hocsinh in db.HOCSINHs select hocsinh;
                foreach (var item in table)
                {
                    if (item.IDLOP == IDLop)
                    {
                        try
                        {
                            var tmp = new HocSinhDTO();
                            tmp.Lop = new LopDTO();
                            tmp.ID = item.IDHS;
                            tmp.Ten = item.TENHS;
                            tmp.GioiTinh = item.GIOITINH;
                            tmp.NgaySinh = item.NGAYSINH.Value;
                            tmp.Lop.ID = item.IDLOP;
                            tmp.Lop.Ten = item.LOP.TENLOP;
                            tmp.DiaChi = item.DIACHI;
                            tmp.Email = item.EMAIL;
                            listDB_HocSinh.Add(tmp);
                        }
                        catch
                        {

                        }
                    }
                    else { }
                }
            }
            return listDB_HocSinh;
        }
        public List<HocSinhDTO> ReadByTenLop(string TenLop)
        {
            var listDB_HocSinh = new List<HocSinhDTO>();
            using (var db = new ProcessData().ConnectionDB())
            {
                var table = from hocsinh in db.HOCSINHs select hocsinh;
                foreach (var item in table)
                {
                    if (item.LOP.TENLOP == TenLop)
                    {
                        try
                        {
                            var tmp = new HocSinhDTO();
                            tmp.Lop = new LopDTO();
                            tmp.ID = item.IDHS;
                            tmp.Ten = item.TENHS;
                            tmp.GioiTinh = item.GIOITINH;
                            tmp.NgaySinh = item.NGAYSINH.Value;
                            tmp.Lop.ID = item.IDLOP;
                            tmp.Lop.Ten = item.LOP.TENLOP;
                            tmp.DiaChi = item.DIACHI;
                            tmp.Email = item.EMAIL;
                            listDB_HocSinh.Add(tmp);
                        }
                        catch
                        {

                        }
                    }
                    else { }
                }
            }
            return listDB_HocSinh;
        }
    }
}
