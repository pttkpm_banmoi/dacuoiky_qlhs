﻿using Fluent;
using QuanLyHocSinhCap3.DAO;
using QuanLyHocSinhCap3.Database;
using QuanLyHocSinhCap3.DTO;
using QuanLyHocSinhCap3.UI;
using QuanLyHocSinhCap3.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ComboBox = System.Windows.Controls.ComboBox;

namespace QuanLyHocSinhCap3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            loadDataItemBaoCao();
            //loadDB_tabctrlQLLOP();

            List<KhoiTreeView> KhoiTree = new List<KhoiTreeView>();
            List<KhoiDTO> tmpKhoi = new List<KhoiDTO>();
            List<LopDTO> tmpLop = new List<LopDTO>();

            tmpKhoi = new KhoiDAO().ReadAll();   

            KhoiTreeView khoi = new KhoiTreeView();

            for (int i = 0; i < new KhoiDAO().ReadAll().Count; i++)
            {
                KhoiTreeView item = new KhoiTreeView() { Ten = tmpKhoi[i].Ten };
                tmpLop = new LopDAO().ReadByTenKhoi(tmpKhoi[i].Ten);
                for (int j = 0; j < tmpLop.Count; j++)
                {
                    item.Members.Add(new LopTreeView() { Ten = tmpLop[j].Ten })
                        ;
                }
                    KhoiTree.Add(item);
            }
            trvFamilies.ItemsSource = KhoiTree;
        }
        /// <summary>
        /// phân quyền xử lý
        /// </summary>
        public MainWindow(TaiKhoanDTO taiKhoan)
        {
            InitializeComponent();
            LopDAO lopDAO = new LopDAO();
            ListLop.ItemsSource = lopDAO.ReadAll();
            MonHocDAO monhocDAO = new MonHocDAO();
            ListMon.ItemsSource = monhocDAO.ReadAll();
            HocKyDAO hockyDAO = new HocKyDAO();
            ListHocKy.ItemsSource = hockyDAO.ReadAll();
            if (taiKhoan.LoaiTaiKhoan.Ten == "Người dùng")
            {
                ShowNguoiDung();
            }
            if (taiKhoan.LoaiTaiKhoan.Ten == "Giáo viên")
            {
                ShowGiaoVien();
            }
            if (taiKhoan.LoaiTaiKhoan.Ten == "Hiệu trưởng")
            {
                ShowHieuTruong();
            }
            loadDataItemBaoCao();
            //loadDB_tabctrlQLLOP();

            List<KhoiTreeView> KhoiTree = new List<KhoiTreeView>();
            List<KhoiDTO> tmpKhoi = new List<KhoiDTO>();
            List<LopDTO> tmpLop = new List<LopDTO>();

            tmpKhoi = new KhoiDAO().ReadAll();

            KhoiTreeView khoi = new KhoiTreeView();

            for (int i = 0; i < new KhoiDAO().ReadAll().Count; i++)
            {
                KhoiTreeView item = new KhoiTreeView() { Ten = tmpKhoi[i].Ten };
                tmpLop = new LopDAO().ReadByTenKhoi(tmpKhoi[i].Ten);
                for (int j = 0; j < tmpLop.Count; j++)
                {
                    item.Members.Add(new LopTreeView() { Ten = tmpLop[j].Ten })
                        ;
                }
                KhoiTree.Add(item);
            }
            trvFamilies.ItemsSource = KhoiTree;
            LoadDataTabItemQLTTCN(taiKhoan);
        }
        private void ShowNguoiDung()
        {

        }
        private void ShowGiaoVien()
        {
            try
            {
                tabThayDoiQuyDinh.IsEnabled = false;
            }
            catch { };
        }
        private void ShowHieuTruong()
        {
            try
            {
                tabThayDoiQuyDinh.IsEnabled = true;
            }
            catch
            {

            }
        }
        /// <summary>
        /// tab control quản lý hoc sinh
        /// </summary>


        private void LoadDataHocSinh()
        {
            HocSinhDAO hocsinhDAO = new HocSinhDAO();
            ListHocSinh.ItemsSource = hocsinhDAO.ReadAll();
        }
        private void LoadDataHocSinh(List<HocSinhDTO> ListHS)
        {
            ListHocSinh.ItemsSource = ListHS;
        }
        HocSinhDTO hocsinhselection = new HocSinhDTO();
        private void ListHocSinh_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGrid DG = sender as DataGrid;
            hocsinhselection = DG.SelectedValue as HocSinhDTO;
            try
            {
                btnCapNhatHocSinh.IsEnabled = true;
                btnXoa.IsEnabled = true;
            }
            catch
            {

            }
        }
        private void btnThemHocSinh(object sender, RoutedEventArgs e)
        {
            ThemSuaHocSinh form = new ThemSuaHocSinh();
            form.OnInsert += Frm_OnInsert;
            form.Show();
        }
        private void Frm_OnInsert(HOCSINH hocsinh)
        {
            new HocSinhDAO().Insert(hocsinh);
            LoadDataHocSinh();
        }
        private void BtnCapNhatHocSinh_Click(object sender, RoutedEventArgs e)
        {
            HOCSINH hocsinh = new HOCSINH();
            hocsinh.IDHS = hocsinhselection.ID;
            hocsinh.TENHS = hocsinhselection.Ten;
            hocsinh.GIOITINH = hocsinhselection.GioiTinh;
            hocsinh.NGAYSINH = hocsinhselection.NgaySinh;
            hocsinh.EMAIL = hocsinhselection.Email;
            hocsinh.IDLOP = hocsinhselection.Lop.ID;
            hocsinh.LOP = new LOP();
            hocsinh.LOP.IDLOP = hocsinhselection.Lop.ID;
            hocsinh.LOP.TENLOP = hocsinhselection.Lop.Ten;
            //hocsinh.
            hocsinh.DIACHI = hocsinhselection.DiaChi;
            ThemSuaHocSinh form = new ThemSuaHocSinh(hocsinh);
            form.OnUpdate += Frm_OnUpdate;
            form.Show();
        }
        private void Frm_OnUpdate(HOCSINH hocsinh)
        {
            string ID = hocsinhselection.ID;
            new HocSinhDAO().Update(ID, hocsinh);
            LoadDataHocSinh();
        }
        private void BtnXoa_Click(object sender, RoutedEventArgs e)
        {
            new HocSinhDAO().Delete(hocsinhselection);
        }
        private void btnCapNhatDuLieu(object sender, RoutedEventArgs e)
        {
            LoadDataHocSinh();
        }
        private void BtnTimKiemByIDHocSinh_Click(object sender, RoutedEventArgs e)
        {
            LoadDataHocSinh(new HocSinhDAO().ReadByIDHocSinh(tbIDHocSinh.Text));
        }

        /// <summary>
        /// tab control quan ly diem
        /// </summary>
        private void LoadDataDiem(List<DiemDTO> listDB)
        {
            listDiem.ItemsSource = listDB;         
        }
        private void BtnChinhSua_Click(object sender, RoutedEventArgs e)
        {
            DIEM diem = new DIEM();
            diem.IDHS = diemselection.HocSinh.ID;
            diem.IDHK = diemselection.HocKy.ID;
            diem.IDMH = diemselection.MonHoc.ID;
            diem.DIEM15P = tbDiem15p.Text;
            diem.DIEM1TIET = tbDiem1tiet.Text;
            diem.DIEMHOCKY = tbDiemThi.Text;
            //SuaDiem form = new SuaDiem(diem);
            //form.OnUpdate += FrmDiem_OnUpdate;
            //form.Show();
            new DiemDAO().Update(diem.IDHS, diem.IDHK, diem.IDMH, diem);
        }
        DiemDTO diemselection = new DiemDTO();
        private void ListDiem_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGrid DG = sender as DataGrid;
            diemselection = DG.SelectedValue as DiemDTO;
            try
            {
                btnChinhSua.IsEnabled = true;
                tbDiem15p.Text = diemselection.Diem15P;
                tbDiem1tiet.Text = diemselection.Diem1Tiet;
                tbDiemThi.Text = diemselection.DiemHocKy;
            }
            catch { }
        }
        private void BtnCapNhat_Click(object sender, RoutedEventArgs e)
        {
            DiemDAO diemDAO = new DiemDAO();
            //diemDAO.Create();
            LoadDataDiem(diemDAO.ReadAll());
        }
        private void FrmDiem_OnUpdate(DIEM diem)
        {
            new DiemDAO().Update(diem.IDHS, diem.IDHK, diem.IDMH, diem);
            //LoadDataDiem();
        }
        DiemDTO diemDTO = new DiemDTO();
        string IDLopSelection = "";
        string IDMonHocSelection = "";
        string IDHocKySelection = "";
        private void BtnTimKiemDiem_Click(object sender, RoutedEventArgs e)
        {
            DiemDAO diemDAO = new DiemDAO();
            var list = new List<DiemDTO>();
            LoadDataDiem(diemDAO.ReadDiemByIDHS_IDMH_IDHK_IDLOP(tbTimKiemHocSinh.Text, IDMonHocSelection, IDHocKySelection, IDLopSelection));
        }
        private void ListLop_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox cb = sender as ComboBox;
            var item = cb.SelectedItem;
            try
            {
                IDLopSelection = ((QuanLyHocSinhCap3.DTO.LopDTO)item).ID.ToString();
            }
            catch { } 
        }
        private void ListMon_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox cb = sender as ComboBox;
            var item = cb.SelectedItem;
            try
            {
                IDMonHocSelection = ((QuanLyHocSinhCap3.DTO.MonHocDTO)item).ID.ToString();
            }
            catch { }
        }
        private void ListHocKy_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox cb = sender as ComboBox;
            var item = cb.SelectedItem;
            try
            {
                IDHocKySelection = ((QuanLyHocSinhCap3.DTO.HocKyDTO)item).ID.ToString();
            }
            catch { }
        }

        /// <summary>
        /// tab control bao cao - tong ket
        /// </summary>     
        String LoaiBaoCao;
        String IDMHSelection;
        String IDHKSelection;
        IList<string> ListBC = new List<string>()
        {
            "Tổng kết môn",
            "Tổng kết học kỳ"
        };
        private void loadDataItemBaoCao()
        {
            ListMonHoc.ItemsSource = new MonHocDAO().ReadAll();
            HocKy.ItemsSource = new HocKyDAO().ReadAll();
            cbBaoCao.ItemsSource = ListBC;
        }
        private void loadDataItemBaoCao(List<BaoCaoDTO> list)
        {
            ListItem.ItemsSource = list;
        }
<<<<<<< HEAD
        private void CbBaoCao_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox cb = sender as ComboBox;
            try
            {
                if (cb.SelectedValue != null)
                {
                    LoaiBaoCao = cb.SelectedValue as string;
                    if (LoaiBaoCao == "Tổng kết môn")
                    {
                        lbTenBaoCao.Content = "BÁO CÁO TỔNG KẾT THEO MÔN HỌC";
                        ListMonHoc.Visibility = Visibility.Visible;
                    }
                    if (LoaiBaoCao == "Tổng kết học kỳ")
                    {
                        lbTenBaoCao.Content = "BÁO CÁO TỔNG KẾT THEO HỌC KỲ";
                        ListMonHoc.Visibility = Visibility.Hidden;
                    }
                }
            }
            catch
            {

            }
            
        }
        private void ListMonHoc_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox cb = sender as ComboBox;
            var item = cb.SelectedItem;
            try
            {
                IDMHSelection = ((QuanLyHocSinhCap3.DTO.MonHocDTO)item).ID.ToString();
            }
            catch { }
        }
        private void HocKy_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox cb = sender as ComboBox;
            var item = cb.SelectedItem;
            try
            {
                IDHKSelection = ((QuanLyHocSinhCap3.DTO.HocKyDTO)item).ID.ToString();
            }
            catch { }
        }
        private void BtnLoad_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                if (LoaiBaoCao == "Tổng kết môn")
                {
                    loadDataItemBaoCao(new BaoCaoMonHocDAO().ReadByIDHK_IDMH(IDHKSelection, IDMHSelection));
                }
                if (LoaiBaoCao == "Tổng kết học kỳ")
                {
                    loadDataItemBaoCao(new BaoCaoMonHocDAO().ReadByIDHK(IDHKSelection));
                }
            }
            catch
            {

            }
            
        }

        /// <summary>
        /// tab control quản lý lớp
        /// </summary>
        private void loadDB_tabctrlQLLOP( List<HocSinhDTO> list)
        {
            dgvTabQuanLyLop.ItemsSource = list;
        }

        private void TrvFamilies_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            TreeView tw = sender as TreeView;
            try
            {
                var item = tw.SelectedValue;
                var ten = ((LopTreeView)item).Ten.ToString();
                var lisths = new HocSinhDAO().ReadByTenLop(ten);
                var lop = new LopDAO().ReadByTenLop(ten);
                lbTenLop.Content = ten;
                lbSiSo.Content = lop.SiSo;
                lbTenGiaoVien.Content = lop.GiaoVien.Ten;
                loadDB_tabctrlQLLOP(lisths);
            }
            catch
            {

            }
            
        }
        ///<summary>
        ///tab quản lý thông tin cá nhân
        ///</summary>
        TaiKhoanDTO user = new TaiKhoanDTO();
        public void LoadDataTabItemQLTTCN(TaiKhoanDTO taikhoan)
        {
            user = taikhoan;
            tblTenTaiKhoan.Text = taikhoan.Ten;
            tblGioiTinh.Text = taikhoan.GioiTinh;
            tblNgaySinh.Text = taikhoan.NgaySinh.ToString("dd/MM/yyyy");
            tblChucVu.Text = taikhoan.LoaiTaiKhoan.Ten;
            tbTen.Text = taikhoan.Ten;
            tbGioiTinh.Text = taikhoan.GioiTinh;
            dpNgaySinh.Text = taikhoan.NgaySinh.ToString();
            tbChucVu.Text = taikhoan.LoaiTaiKhoan.Ten;
        }
        private void TblThongTinTaiKhoan_Click(object sender, RoutedEventArgs e)
        {
            CardThongTin.Visibility = Visibility.Visible;
            CardDoiMatKhau.Visibility = Visibility.Hidden;
        }

        private void TblDoiMatKhau_Click(object sender, RoutedEventArgs e)
        {
            CardThongTin.Visibility = Visibility.Hidden;
            CardDoiMatKhau.Visibility = Visibility.Visible;
        }

        private void TblDangXuat_Click(object sender, RoutedEventArgs e)
        {
            login form_login = new login();
            form_login.Show();
            this.Close();
        }
        private void BtnSaveMatKhau_Click(object sender, RoutedEventArgs e)
        {
            new TaiKhoanDAO().UpdatePassWord(user, PasswordBox.Password);
        }

        private void BtnLuu_Click(object sender, RoutedEventArgs e)
        {
            user.Ten = tbTen.Text;
            user.GioiTinh = tbGioiTinh.Text;
            user.NgaySinh = DateTime.Parse(dpNgaySinh.Text);
            new TaiKhoanDAO().Update(user.TenDangNhap, user);
            LoadDataTabItemQLTTCN(user);
        }

        /// <summary>
        /// tabitem Thay Đổi quy Định
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DgvListLoptabThayDoiQuyDinh_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGrid DG = sender as DataGrid;
            var lophocselection = DG.SelectedValue as LopDTO;
            try
            {
                lbTenLopHoc.Content = lophocselection.Ten;
                tbSiSoMax.Text = lophocselection.SiSoMax.ToString();
            }
            catch
            {

            }
        }

        private void BtnThayDoiSiSo_Click(object sender, RoutedEventArgs e)
        {
            dgvListLoptabThayDoiQuyDinh.ItemsSource = new LopDAO().ReadAll();
        }

        private void btnLuuSiSoMax(object sender, RoutedEventArgs e)
        {

        }

        
    }
    public class KhoiTreeView
    {
        public KhoiTreeView()
        {
            this.Members = new ObservableCollection<LopTreeView>();
        }

        public string Ten { get; set; }

        public ObservableCollection<LopTreeView> Members { get; set; }
    }

    public class LopTreeView
    {
        public string Ten { get; set; }
=======

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Doimatkhau d = new Doimatkhau();
            d.Show();
            this.Hide();
        }
>>>>>>> 43c6506e95414fba4a937604bee136a1104a6885
    }
}